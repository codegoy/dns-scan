package main

import (
	"context"
	"flag"
	"fmt"
	"net"
	"sync"
	"time"
)

type Lookup struct {
	Range    string
	Name     string
	IP       string
	Timeout  int
	Output   [255]string
	DNS      string
	Resolver net.Resolver
}

func (l *Lookup) LookupNameRange(hostID int) (string, error) {
	target := fmt.Sprintf("%s%d", l.Range, hostID)
	if rv, err := l.Resolver.LookupAddr(context.Background(), target); err != nil {
		return "", fmt.Errorf("ERROR::%s::%v\n", target, err)
	} else {
		return fmt.Sprintf("%s -> %s", target, rv[0]), nil
	}
}

func (l *Lookup) LookupName() (string, error) {
	if rv, err := l.Resolver.LookupAddr(context.Background(), l.IP); err != nil {
		return "", fmt.Errorf("ERROR::%s::%v\n", l.IP, err)
	} else {
		return fmt.Sprintf("%s -> %s", l.IP, rv[0]), nil
	}
}

func (l *Lookup) LookupIPs(target string) (string, error) {
	if rv, err := l.Resolver.LookupIPAddr(context.Background(), target); err != nil {
		return "", fmt.Errorf("ERROR::%s::%v\n", target, err)
	} else {
		var r string
		for _, v := range rv {
			r = r + fmt.Sprintf("%s -> %s\n", target, v.String())
		}
		return r[:len(r)-1], nil
	}
}

func main() {
	l := Lookup{}
	flag.StringVar(&l.Range, "r", "", "Scan Network ID range; example: 10.0.0.")
	flag.StringVar(&l.IP, "a", "", "Name lookup from IP; example: 10.0.0.2")
	flag.StringVar(&l.Name, "i", "", "IP lookup from Name; example: duckduckgo.com")
	flag.StringVar(&l.DNS, "d", "", "DNS server ipaddress; example: 10.0.0.1")
	flag.IntVar(&l.Timeout, "t", 10, "Connection timeout in seconds")
	flag.Parse()
	l.Resolver = net.Resolver{
		PreferGo: true,
		Dial: func(ctx context.Context, network, address string) (net.Conn, error) {
			d := net.Dialer{
				Timeout: time.Duration(l.Timeout) * time.Second,
			}
			return d.DialContext(ctx, "udp", l.DNS+":53")
		},
	}
	if l.Name != "" {
		if r, err := l.LookupIPs(l.Name); err == nil {
			fmt.Println(r)
		}
		return
	}
	if l.IP != "" {
		if n, err := l.LookupName(); err == nil {
			fmt.Println(n)
		}
		return
	}
	if l.Range != "" {
		if l.Range[len(l.Range)-1:] != "." {
			l.Range = l.Range + "."
		}
		var wg sync.WaitGroup
		for x := 0; x < 254; x++ {
			wg.Add(1)
			x := x
			go func() {
				if r, err := l.LookupNameRange(x); err == nil {
					l.Output[x] = r
				}
				wg.Done()
			}()
		}
		wg.Wait()
		for _, v := range l.Output {
			if v != "" {
				fmt.Println(v)
			}
		}
		return
	}
}
